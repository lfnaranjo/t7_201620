package taller.interfaz;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Scanner;
import java.util.Properties;

import taller.Arbol;
import taller.Reconstructor;
import taller.Arbol.Node;

public class ArbolCLI 
{

	private Scanner in;
	private      Reconstructor x;

	private static final String ruta = "C:/Users/TOSHIBA/Desktop/3ro/estructuras/tllerres_5_pa_arriba/t7_201620/taller7/data/";///sirve

	public ArbolCLI()
	{
		in = new Scanner(System.in);
		x=new Reconstructor();
	}

	public void mainMenu()
	{
		boolean finish = false;
		while(!finish)
		{	
			Screen.clear();
			System.out.println("------------------------------------------");
			System.out.println("-                                        -");
			System.out.println("-           Siembra de árboles           -");
			System.out.println("-                                        -");
			System.out.println("------------------------------------------");
			System.out.println("EL sistema para la plantación de árboles binarios\n");

			System.out.println("Menú principal:");
			System.out.println("-----------------");
			System.out.println("1. Cargar archivo con semillas");
			System.out.println("2. Salir");
			System.out.print("\nSeleccione una opción: ");
			int opt1 = in.nextInt();

			switch(opt1)
			{
			case 1:
				recibirArchivo();
				finish = true;
				break;
			case 2:
				finish = true;
				break;
			default:
				break;
			}
		}
	}

	public void recibirArchivo()
	{
		boolean finish = false;
		while(!finish)
		{
			Screen.clear();
			System.out.println("Recuerde que el archivo a cargar");
			System.out.println("debe ser un archivo properties");
			System.out.println("que tenga la propiedad in-orden,");
			System.out.println("la propiedad pre-orden (donde los ");
			System.out.println("elementos estén separados por comas) y");
			System.out.println("que esté guardado en la carpeta data.");
			System.out.println("");
			System.out.println("Introduzca el nombre del archivo:");
			System.out.println("----------------------------------------------------");
			String archivo=in.next();
			// TODO Leer el archivo .properties 

			try {


				x.cargarArchivo(ruta+archivo+".properties");
				// TODO cargarArchivo
				// TODO Reconstruir árbol  
				x.reconstruir();
				x.crearArchivo("./data/"+archivo +".json");
				System.out.println("Ha plantado el árbol con éxito!\nPara verlo, dirijase a /data/arbolPlantado.json");


Reconstructor ReconsMain =new Reconstructor();
ReconsMain.cargarArchivo("./data/Arbol.properties");
Arbol principal=ReconsMain.gettree();
Reconstructor ReconsSUb =new Reconstructor();
ReconsSUb.cargarArchivo("./data/SubArbol.properties");
Arbol sub=ReconsMain.gettree();
System.out.println("esta el apellido dentro del nombre"+essub(principal, sub));


				System.out.println("Nota: ejecute Refresh (Clic derecho - Refresh) sobre la carpeta /data/ para actualizar y visualizar el archivo JSON");







				System.out.println("Presione 1 para salir");
				in.next();
				finish=true;




			} catch (Exception e) {
				System.out.println("Hubo un problema cargando el archivo:");
				System.out.println(e.getMessage());
				e.printStackTrace();

			}
		}
	}

	public String essub(Arbol T,Arbol S)

	{
		if(esSubArbol(T.getRoot(), S.getRoot()))
				{
			return "si";
				}
		else return "no";

	}
	public boolean iguales(Node root1, Node root2) 
	{


		if (root1 == null && root2 == null)
			return true;

		if (root1 == null || root2 == null)
			return false;

		return (root1.getKey() == root2.getKey()
				&& iguales(root1.getLeft(), root2.getLeft())
				&& iguales(root1.getRight(), root2.getRight()));
	}


	public boolean esSubArbol(Node T, Node S) 
	{
		if (S == null) 
			return true;

		if (T == null)
			return false;

		if (iguales(T, S)) 
			return true;


		return esSubArbol(T.getLeft(), S)
				|| esSubArbol(T.getRight(), S);
	}


}
