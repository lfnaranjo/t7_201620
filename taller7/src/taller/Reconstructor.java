package taller;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.Properties;

import taller.Arbol.Node;
import taller.interfaz.IReconstructorArbol;





import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.opencsv.CSVReader;

public class Reconstructor		 implements IReconstructorArbol
{


	private Properties config;
	private String inorder;
	private String preorder;
	private Arbol  tree;


	public Reconstructor()
	{	
		tree=new Arbol<String>();

		//cargarArchivo(nombre);	
	}

	@Override
	public void cargarArchivo(String nombre) throws IOException 
	{

		FileInputStream fis = new FileInputStream(nombre );
		config = new Properties( );
		config.load( fis );
		fis.close( );	


		String x=config.getProperty("preorden");
		String y=config.getProperty("inorden");

		String[] v=y.split(";");
		String[] w=y.split(";");
		for(int i=0; i<w.length;i++)
		{
			preorder+=v[i];
			inorder+=v[i];
		}

		// TODO Auto-generated method stub
	}

	@Override
	public void crearArchivo(String ruta) throws FileNotFoundException, UnsupportedEncodingException
	{


		Gson json = new GsonBuilder().setPrettyPrinting().create();
		String s=json.toJson(tree, Arbol.class);

		try(  PrintWriter out = new PrintWriter( ruta )  ){
			out.println( s );
		}

		catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void reconstruir() 
	{
		tree.setRoot(Reconstruir(preorder, inorder));
		// TODO Auto-generated method stub
	}



	public Node Reconstruir( String preord ,String inord ) 
	{

		if(preord==null ||preord==" " || inord ==null || inord==" ")
		{
			return null;
		}

		String val=preord.substring(0,1);
		Node tnode=tree.createNode(val);



		String inizq=inord.substring(0,inord.indexOf(val) ); 

		String  inder=inord.substring( inord.indexOf(val)+1);

		String  preizq;
		String preder;


		if(inizq!=null ||inizq!=" " )
		{
			preizq=preord.substring(1,inizq.length()+1 );
			preder=preord.substring(inizq.length()+1); 	
		}
		else
		{
			preizq=preord.substring(1 );
			preder=preord.substring(1); 
		}

		tnode.putLeft(Reconstruir(preizq,inizq ));
		tnode.putRight(Reconstruir(preder,inder));

		return tnode;
	}

	public void setInorder(String inorder1)
	{
		inorder=inorder1;	
	}
	public void setPreoorder(String preoroder1)
	{
		preorder=preoroder1;
	}

	public String getInorder()
	{
		return inorder;
	}

	public String getPreorder()
	{
		return preorder;
	}
	public Arbol gettree()
	{
		return tree;
	}



}