package taller.test;

import java.io.IOException;

import taller.Reconstructor;
import junit.framework.TestCase;

public class MainTest extends TestCase
{
	private Reconstructor  recons;

	private void setupEscenario1( )
	{
		recons= new    Reconstructor();
	}
	public void testcargar() 
	{
		setupEscenario1();
		try {
			recons.cargarArchivo( "./data/Prueba1.properties");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		assertEquals(recons.getInorder(),"DBEAFCG" );
		assertEquals(recons.getPreorder(),"ABDECFG" );

		try {
			recons.cargarArchivo( "./data/Prueba2.properties");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		assertEquals(recons.getInorder(),"KLJHQOP");
		assertEquals(recons.getPreorder(),"JKLOQHP" );

	}

	private void setupEscenario2( )

	{
		setupEscenario1();
		try {
			recons.cargarArchivo( "./data/Prueba1.properties");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public void testReconstruir() 
	{
		setupEscenario2();
		recons.reconstruir();

		assertEquals(recons.gettree().getRoot().getKey(),"A" );
		assertEquals(recons.gettree().getRoot().getLeft().getLeft().getKey(),"	D" );
		assertEquals(recons.gettree().getRoot().getLeft().getRight().getKey(),"E" );
		assertEquals(recons.gettree().getRoot().getRight().getKey(),"C" );
		

		
	}
	
	private void setupEscenario3( )

	{
		setupEscenario1();
		try {
			recons.cargarArchivo( "./data/Prueba2.properties");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void testReconstruir2() 
	{
		setupEscenario3();
		recons.reconstruir();

		assertEquals(recons.gettree().getRoot().getKey(),"J" );
		assertEquals(recons.gettree().getRoot().getLeft().getLeft().getKey(),null );
		assertEquals(recons.gettree().getRoot().getLeft().getRight().getKey(),"L" );
		assertEquals(recons.gettree().getRoot().getRight().getKey(),"O" );
		

		
	}


}