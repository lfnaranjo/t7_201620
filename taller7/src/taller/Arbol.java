package taller;

public class Arbol  <Key>
{	
	private Node root; // root of BST
	public class Node
	{
		private Key key; // key
		// associated value
		private Node left, right; // links to subtrees
		public Node(Key key)
		{
			this.key = key;
		}
		public void putRight(Node node)
		{
			this.right=node;
		}
		public void putLeft(Node node)
		{
			this.left=node;	
		}
		public void putKey(Key key)
		{
			this.key=key;
		}
		public Node getLeft()
		{
		return left;	
		}
		public Node getRight()
		{
		return right;	
		}
		
		public Key getKey()
		{
			return key;
		}
	}

	public Arbol()
	{
		
	}

public Node getRoot()
{
return 	root;
}
public void setRoot(Node node)
{
	root=node;
	}

public Node createNode(Key key)
{
return new Node(key);
}



}